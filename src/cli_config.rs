use std::{convert::TryFrom, path::PathBuf};

use clap::{App, Arg, ArgMatches};
use itertools::{chain, izip, Itertools};

use crate::util::OptIter;

/// Stores the configuration retrieved from the command-line arguments.
#[derive(Debug)]
pub struct Config {
    /// Ordered list of actions to perform.
    pub actions: Vec<Action>,
    /// Whether the interactive shell has to be run or not.
    pub run_interactive_shell: bool,
    /// List of extra import paths for the runtime 'import()' function.
    pub extra_import_paths: Vec<PathBuf>,
    /// Color mode to apply.
    pub color_mode: ColorMode,
}

/// Represents an action to perform given in the cli arguments.
#[derive(Debug, Clone)]
pub enum Action {
    /// Include the given file.
    Include(PathBuf),
    /// Import the given module (same as include, but use import directories).
    Import(String),
    /// Execute the given lola code.
    Execute(String),
}

/// Which color mode has been asked in the cli arguments.
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum ColorMode {
    Auto,
    ForceColor,
    ForceNoColor,
}

impl ColorMode {
    pub fn should_we_colorize(&self) -> bool {
        match self {
            ColorMode::Auto => atty::is(atty::Stream::Stdout),
            ColorMode::ForceColor => true,
            ColorMode::ForceNoColor => false,
        }
    }
}

impl TryFrom<&ArgMatches<'_>> for ColorMode {
    type Error = String;

    fn try_from(matches: &ArgMatches) -> Result<ColorMode, String> {
        let color = matches.value_of("color");
        let has_force_color = matches.is_present("force-color");
        let has_force_no_color = matches.is_present("no-color");
        let has_color = color.is_some();

        let nb_options: i8 = [has_force_color, has_force_no_color, has_color]
            .iter()
            .map(|b| i8::from(*b))
            .sum();
        if nb_options > 1 {
            return Err("Conflicting color options".to_string());
        }

        let mode = if has_force_color {
            Self::ForceColor
        } else if has_force_no_color {
            Self::ForceNoColor
        } else {
            match color {
                Some("always") => Self::ForceColor,
                Some("never") => Self::ForceNoColor,
                None | Some("auto") => Self::Auto,
                Some(_) => unreachable!(), // clap handles the possible values
            }
        };

        Ok(mode)
    }
}

/// Returns the command-line argument definition for clap.
fn cli_definition<'a, 'b>() -> App<'a, 'b> {
    App::new("lola")
        .version("0.2.0")
        .about("A simple language based on units of measurement")
        .arg(
            Arg::with_name("import")
                .short("i")
                .long("import")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1)
                .help("Import the given module(s) before running the repl"),
        )
        .arg(
            Arg::with_name("include")
                .short("I")
                .long("include")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1)
                .help("Include the given script(s) before running the repl"),
        )
        .arg(
            Arg::with_name("execute")
                .short("e")
                .long("execute")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1)
                .help("Execute the given expressions before running the repl"),
        )
        .arg(
            Arg::with_name("no-interactive")
                .short("n")
                .long("no-interactive")
                .help("If present, don't run the interactive shell"),
        )
        .arg(
            Arg::with_name("force-color")
                .short("c")
                .long("force-color")
                .help("Shortcut for --color=always"),
        )
        .arg(
            Arg::with_name("no-color")
                .short("C")
                .long("no-color")
                .help("Shortcut for --color=never"),
        )
        .arg(
            Arg::with_name("color")
                .long("color")
                .takes_value(true)
                .possible_values(&["always", "never", "auto"])
                .use_delimiter(false)
                .help("Shortcut for --color=always"),
        )
        .arg(
            Arg::with_name("import-path")
                .short("P")
                .long("import-path")
                .takes_value(true)
                .multiple(true)
                .number_of_values(1)
                .help("Add the given include directory(ies) for the runtime 'import()' function"),
        )
}

/// Retrieves the mixed options `-i`, '-I' and `-e` in the order they were defined in the arguments.
fn collect_actions(matches: &ArgMatches) -> Vec<Action> {
    // Helper to create iterator over `(usize, Action)`, where the `usize` is the index of the
    // argument.
    fn indexed_args_iter<I, A, F>(
        indices: Option<I>,
        args: Option<A>,
        f: F,
    ) -> impl Iterator<Item = (usize, Action)>
    where
        I: Iterator<Item = usize>,
        A: Iterator,
        F: Fn(<A as Iterator>::Item) -> Action,
    {
        let inner = match (indices, args) {
            (Some(indices), Some(args)) => Some(izip!(indices, args.map(f))),
            _ => None,
        };
        OptIter::new(inner)
    }

    macro_rules! indexed_args_iter {
        ($arg:expr, $map_closure:expr) => {
            indexed_args_iter(
                matches.indices_of($arg),
                matches.values_of($arg),
                $map_closure,
            )
        };
    }

    let (import_args, include_args, execute_args) = (
        indexed_args_iter!("import", |f| Action::Import(String::from(f))),
        indexed_args_iter!("include", |f| Action::Include(PathBuf::from(f))),
        indexed_args_iter!("execute", |f| Action::Execute(String::from(f))),
    );

    chain!(import_args, include_args, execute_args)
        .sorted_by_key(|k| k.0) // sorted by index in the cli arguments
        .map(|(_index, action)| action)
        .collect::<Vec<_>>()
}

impl Config {
    pub fn new() -> Config {
        let matches = cli_definition().get_matches();

        let extra_import_paths = match matches.values_of_os("import-path") {
            Some(dir_iter) => dir_iter.map(PathBuf::from).collect(),
            None => vec![],
        };

        let color_mode = match ColorMode::try_from(&matches) {
            Ok(mode) => mode,
            Err(msg) => {
                eprintln!("{}", msg);
                std::process::exit(1);
            }
        };

        Self {
            actions: collect_actions(&matches),
            run_interactive_shell: !matches.is_present("no-interactive"),
            extra_import_paths,
            color_mode,
        }
    }
}
