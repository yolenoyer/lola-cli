use std::path::PathBuf;

use anyhow::{anyhow, Result};
use rustyline::{Editor, Helper};

use crate::get_project_file;

/// Name of the readline history file, stored in the user project data directory.
/// E.g., for linux the whole path will be `$HOME/.local/share/lola/history`.
const READLINE_HISTORY_FILE: &str = "history";

/// Loads the readline history file if present.
pub fn setup_history<H: Helper>(rl: &mut Editor<H>) -> Result<()> {
    rl.load_history(&get_history_file()?).or(Ok(()))
}

/// Saves the readline history.
pub fn save_history<H: Helper>(rl: &mut Editor<H>) -> Result<()> {
    let history_file = get_history_file()?;
    rl.save_history(&history_file)
        .map_err(|_| anyhow!("Unable to save the history into {}", history_file.display()))
}

/// Returns the whole path to the readline history file.
fn get_history_file() -> Result<PathBuf> {
    get_project_file(|d| d.data_local_dir(), READLINE_HISTORY_FILE)
        .ok_or_else(|| anyhow!("Unable to get the path to the history file"))
}
