use std::fs;
use std::path::Path;
use std::path::PathBuf;
use std::process;

use anyhow::{anyhow, Result};

use lola::context::{Container, ContextRef, DefaultImporter, Scope};
use lola::library::prefill_context;
use lola::parse::{display_error, parse_expr};

use crate::cli_config::{Action, Config};
use crate::config_file::ConfigFile;
use crate::formatted_value::formatted_value;

/// Processes some input coming from the interactive shell or from a file buffer.
pub fn process_input(input: &str, cxt: &mut ContextRef, colored: bool) -> Result<Option<String>> {
    match parse_expr(input, cxt.container, None) {
        Ok(expr) => match expr.eval(cxt) {
            Ok(value) => {
                cxt.scope
                    .get_var_mut("_", |underscore| *underscore = value.clone())
                    .unwrap();
                if value.is_void() {
                    Ok(None)
                } else {
                    Ok(Some(formatted_value(&value, colored, cxt.container.format_config())))
                }
            }
            Err(err) => Err(anyhow!(err.display_with_source(cxt.container))),
        },
        Err(err) => Err(anyhow!(display_error(&err))),
    }
}

/// Creates the container, with native function and units, and by evaluating the expressions and
/// files given in the command arguments.
pub fn create_context(
    config: &Config,
    config_file: &ConfigFile,
    colored: bool,
) -> Result<(Container, Scope, Option<String>)> {
    // Create the module importer:
    let mut importer = DefaultImporter::new();
    for path in &config_file.import_paths {
        add_import_path(&mut importer, PathBuf::from(path));
    }
    for path in &config.extra_import_paths {
        add_import_path(&mut importer, path.to_owned());
    }

    // Create the context:
    let mut container = Container::new(Container::default_writer(), importer);
    let mut scope = Scope::new();
    let mut cxt = ContextRef::new(&mut container, &mut scope);
    prefill_context(&mut cxt);

    // Import module from the configuration file:
    for import_name in &config_file.imports {
        if let Err(err) = import_module(import_name, &mut cxt) {
            eprintln!("{}", err);
        }
    }

    let mut result = Ok(None);

    // Execute actions to perform, in the order they were defined in the arguments:
    for action in &config.actions {
        result = match action {
            Action::Import(module_name) => import_module(module_name, &mut cxt),
            Action::Include(file) => parse_file(file, &mut cxt, colored),
            Action::Execute(expr) => execute_expr(expr, &mut cxt, colored),
        };
        if let Err(err) = result {
            eprintln!("{}", err);
            process::exit(1);
        }
    }

    Ok((container, scope, result.unwrap()))
}

/// Imports a module.
fn import_module(import_name: &str, cxt: &mut ContextRef) -> Result<Option<String>> {
    let importer = cxt.container.importer().borrow();
    let import = importer
        .get_code(import_name, None)
        .map_err(|err| anyhow!("{}", err))?;
    drop(importer);

    let expr = parse_expr(
        &import.0,
        cxt.container,
        Some((import_name.to_string(), import.1.clone())),
    )
    .map_err(|err| {
        anyhow!(
            "Unable to parse the module '{}' ({}): {}",
            import_name,
            import.1.as_ref().unwrap().display(),
            display_error(&err)
        )
    })?;

    expr.eval(cxt)
        .map_err(|err| anyhow!("{}", err.display_with_source(cxt.container)))?;

    Ok(None)
}

/// Parses a file and add the parsed definitions into the container.
fn parse_file(filename: &Path, cxt: &mut ContextRef, colored: bool) -> Result<Option<String>> {
    let data = fs::read_to_string(filename)
        .map_err(|_| anyhow!("Unable to open the file '{}'.", filename.display()))?;
    process_input(data.as_ref(), cxt, colored)
}

/// Executes an expression given in command arguments (option `-e`).
fn execute_expr(expr: &str, cxt: &mut ContextRef, colored: bool) -> Result<Option<String>> {
    process_input(expr, cxt, colored)
}

/// Adds a path to the given importer, while expanding the tilde character and env variables if
/// necessary.
fn add_import_path(importer: &mut DefaultImporter, path: PathBuf) {
    // No choice, we have to convert the PathBuf to of &str:
    let path_as_str = match path.to_str() {
        Some(s) => s,
        None => {
            eprintln!(
                "Warning: import path '{}' is not UTF-8, ignoring.",
                path.display()
            );
            return;
        }
    };

    // Expand tilde and env variables:
    let path = match shellexpand::full(&path_as_str) {
        Ok(path) => path,
        Err(_) => {
            eprintln!(
                "Warning: error while trying to expand path '{}', ignoring.",
                path.display()
            );
            return;
        }
    };

    // Add the import path:
    importer.add_import_path(PathBuf::from(path.into_owned()));
}
