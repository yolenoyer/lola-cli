use rustyline::completion::{extract_word, Completer, Pair};
use rustyline::error::ReadlineError;
use rustyline::{Context as RustyContext, Editor};
use rustyline_derive::{Helper, Highlighter, Hinter, Validator};

use lola::context::{ContextRef, Scope};

use crate::eval::process_input;
#[cfg(feature = "readline-history")]
use crate::readline_history::{save_history, setup_history};

/// The readline prompt.
const RL_PROMPT: &str = ">> ";

/// Chars detected as a break for shell completion.
const BREAK_CHARS: &[u8] = &[
    b' ', b'\t', b'\n', b'"', b'\\', b'\'', b'`', b'@', b'$', b'>', b'<', b'=', b';', b'|', b'&',
    b'{', b'(', b'\0', b'+', b'-', b'%', b'*', b'/', b'^', b'(', b')', b'=', b'~', b'!', b'{',
    b'}', b'(', b')', b'[', b']', b':', b',',
];

#[derive(Debug)]
pub struct Readline(Editor<ReadlineHelper>);

/// A wrapper around the rustyline `Editor`.
impl Readline {
    pub fn new(cxt: &ContextRef) -> Self {
        let mut rl = Editor::new();
        let helper = ReadlineHelper {
            scope: cxt.scope as *const Scope,
        };
        rl.set_helper(Some(helper));

        #[cfg(feature = "readline-history")]
        if let Err(err) = setup_history(&mut rl) {
            eprintln!("{}", err);
        }

        Self(rl)
    }

    pub fn read(&mut self) -> Result<String, ReadlineError> {
        self.0.readline(RL_PROMPT)
    }

    #[cfg(feature = "readline-history")]
    pub fn add_history_entry(&mut self, line: &str) {
        self.0.add_history_entry(line);
    }
}

#[cfg(feature = "readline-history")]
impl Drop for Readline {
    fn drop(&mut self) {
        if let Err(err) = save_history(&mut self.0) {
            eprintln!("{}", err);
        }
    }
}

#[derive(Helper, Highlighter, Validator, Hinter)]
struct ReadlineHelper {
    scope: *const Scope,
}

impl Completer for ReadlineHelper {
    type Candidate = Pair;

    fn complete(
        &self,
        line: &str,
        pos: usize,
        _ctx: &RustyContext<'_>,
    ) -> Result<(usize, Vec<Pair>), ReadlineError> {
        let (start_pos, word) = extract_word(line, pos, None, BREAK_CHARS);
        let vars = unsafe {
            (*self.scope)
                .vars_starting_with(word)
                .into_iter()
                .map(|s| Pair {
                    display: "".to_string(),
                    replacement: s,
                })
                .collect::<Vec<_>>()
        };
        Ok((start_pos, vars))
    }
}

/// Processes one line entered in the interactive shell.
pub fn process_interactive_line(line: &str, cxt: &mut ContextRef, colored: bool) {
    let message = match process_input(line, cxt, colored) {
        Ok(output) => match output {
            Some(msg) => msg,
            None => return,
        },
        Err(err) => err.to_string(),
    };
    println!("{}", message);
}
