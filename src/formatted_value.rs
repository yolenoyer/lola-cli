use colored::*;
use itertools::Itertools;

use lola::util::numbers::format_number;
use lola::value::{Value, FormatConfig, Format};

const TAB_SIZE: usize = 2;
const MAX_WIDTH: usize = 60;

pub fn formatted_value(value: &Value, colored: bool, format_config: &FormatConfig) -> String {
    real_formatted_value(value, colored, 0, format_config)
}

fn real_formatted_value(value: &Value, colored: bool, indent: usize, format_config: &FormatConfig) -> String {
    macro_rules! colored {
        ($fmt:literal, $v:expr, $($color:ident),*) => {
            format!($fmt, $v)$(.$color())*.to_string()
        }
    }
    macro_rules! numeric {
        ($v: expr) => { colored!("{}", $v, magenta, bold) };
    }
    macro_rules! string {
        ($v: expr) => { colored!("{}", $v, green, bold) };
    }
    macro_rules! unit {
        ($v: expr) => { colored!("{}", $v, yellow, bold) };
    }
    macro_rules! array_sign {
        ($v: expr) => { colored!("{}", $v, bold) };
    }

    let formatted = match value {
        Value::Void(_) => format!(""),
        Value::Num(operand) => {
            let (num, unit) = (format_number(operand.num(), Some(format_config)), operand.unit());
            format!("{} {}", numeric!(num), unit!(unit))
        }
        Value::Int(v) => numeric!(v),
        Value::Boolean(v) => numeric!(v),
        Value::Str(v) => string!(v.format(format_config, true)),
        Value::Func(v) => v.to_string(),
        Value::Dict(_) => "[dict]".to_string(),
        Value::Array(v) => {
            const SEPAR: &str = ", ";
            const OPEN_SIGN: &str = "[";
            const CLOSE_SIGN: &str = "]";

            let values = v.values();
            let values = values.borrow();

            let mut values_size = values
                .iter()
                .map(|v| v.to_string())
                .fold(0, |acc, v| acc + v.len());
            values_size += 4 + (values.len().max(1) - 1) * SEPAR.len() + tab_size(indent);

            let open_sign = array_sign!(OPEN_SIGN);
            let close_sign = array_sign!(CLOSE_SIGN);

            if values_size <= MAX_WIDTH {
                let values = values
                    .iter()
                    .map(|v| formatted_value(v, colored, format_config))
                    .join(SEPAR);
                format!("{} {} {}", open_sign, values, close_sign)
            } else {
                let values = values
                    .iter()
                    .map(|v| real_formatted_value(v, colored, indent + 1, format_config))
                    .join(",\n");
                format!(
                    "{}\n{}\n{}{}",
                    open_sign,
                    values,
                    get_indent(indent),
                    close_sign
                )
            }
        }
    };

    format!("{}{}", get_indent(indent), formatted)
}

#[inline]
fn get_indent(indent: usize) -> String {
    " ".repeat(tab_size(indent))
}

#[inline]
fn tab_size(indent: usize) -> usize {
    indent * TAB_SIZE
}
