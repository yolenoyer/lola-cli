// To see a quick demo of this script, run the following command in the root
// project path:
//
//    $ cargo run -- -nI ./examples/filter-map.lola -e 'demo()'
//
// To use it in an interactive shell, run:
//
//    $ cargo run -- -I ./examples/filter-map.lola
//
// Then inside the interactive prompt, run:
//
//    > demo()

// Returns a mapped copy of the given array, by applying the mapper function on each element.
fn map(arr: array, mapper: function): array {
    let new_arr = [];
    for v in arr {
        new_arr += [ mapper(v) ]
    }
}

// Return a filtered copy of the given array, by testing the given predicate on each element.
fn filter(arr: array, pred: function): array {
    let new_arr = [];
    for v in arr {
        if pred(v) new_arr += [v]
    }
    new_arr
}

fn demo() {
    let a = [ -13, 0, 7, 12, 20, 30 ];
    print("This is a demo which shows how to use callbacks to create powerful functions.");
    print("It recreates the typical functions map() and filter().");
    print("");
    print("Original array:", a);

    let f = filter(a, fn(n) abs(n) > 10);
    print("Filter absolute values greater than 10:", f);

    let m = map(a, fn(n) n * 10);
    print("Map values (multiplied by 10):", m);
}
