# lola-cli

`lola-cli` is the command-line interface for the
[lola library](https://gitlab.com/yolenoyer/lola). It can run in several modes:

# Interactive mode:

Interactive mode is the default (when the `-n` option is **not** present) : it will run an
interactive command line, where you can run lola expressions.

## Interactive mode : basic example

#### Run in shell:
```
$ lola
>> 1 + 1
2
>> <Ctrl+D>
Bye
$
```

## Interactive mode : adding pre-requisite expressions:

Option `-e EXPRESSION` will run the given expression before running the interactive command-line.

TIP: multiple `-e` options can be added to the command line.

#### Run in shell:
```
$ lola -e 'a = 45km'
>> a * 2
90 km
>>
```

## Interactive mode : including a file:

Instead of adding complex expressions with `-e`, you can include a whole file by using the `-i
INCLUDE_FILE` option.

#### File example to include: `~/fibonacci.lola`
```
fn fibo(n) {
    let terms = [];
    let a = 1;
    let b = 1;
    while n-- > 0 {
        terms += a;
        let next = a + b;
        a = b;
        b = next;
    };
    terms
}
```

#### Run in shell:
```
$ lola -i ~/fibonacci.lola
>> fibo(10)
[ 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 ]
>>
```

# Non-interactive mode (`-n` option):

Non-interactive mode is activated with the `-n` option.

Once inside non-interactive mode, at least one of the below options is needed in order to output
anything:

 - `-i INCLUDE_FILE` : read the given file and execute it;
 - `-e EXPRESSION` : execute the given expression.

Multiple `-i` and `-e` options can be given, they will be processed in the given order. The last
given expression will be displayed.

TIP: if the last expression is void (for example: the last character is a semi-colon), then no
result will be printed at all.

## Non-interactive mode example : executing an single expression:

#### Run in shell:
```
$ lola -ne '3km * 600m'
1.8 km2
$
```

## Non-interactive mode example : including a file before executing an single expression:

#### File example to include: `~/fibonacci.lola`
```
fn fibo(n) {
    let terms = [];
    let a = 1;
    let b = 1;
    while n-- > 0 {
        terms += a;
        let next = a + b;
        a = b;
        b = next;
    };
    terms
}
```

#### Run in shell:
```
$ lola -ni ~/fibonacci.lola -e 'fibo(10)'
[ 1, 1, 2, 3, 5, 8, 13, 21, 34, 55 ]
$
```
